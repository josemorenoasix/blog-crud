<?php

class MySqlConnection extends DbConnection
{

    /**
     * This will be the mysqli object
     * @var mysqli
     */
    private $conn = "";

    /**
     * MySqlConnection constructor.
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $db
     */
    public function __construct($host, $user, $pass, $db)
    {
        parent::__construct($host, $user, $pass, $db);
    }

    /**
     * Tries to make a new connection to database
     * @return bool
     */
    public function connect(): bool
    {
        if ($this->isConnected) {
            return true;
        }
        $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db);
        if ($this->conn->connect_errno > 0) {
            array_push($this->result, $this->conn->connect_error);
            $this->isConnected = false;
            return false;
        }
        $this->conn->set_charset("utf8");
        $this->isConnected = true;
        return true;
    }

    /**
     * Tries to disconnect from database
     * @return bool
     */
    public function disconnect(): bool
    {
        if ($this->isConnected) {
            if ($this->conn->close()) {
                $this->isConnected = false;
                return true;
            }
        }
        return false;
    }

    /**
     * @return mysqli
     */
    public function getConn(): mysqli
    {
        //debug_print_backtrace();
        return $this->conn;
    }

}