<?php

/**
 * Class PdoConnection
 * Info source: https://phpdelusions.net/pdo
 */

class PdoConnection extends DbConnection
{
    protected $host;
    protected $user;
    protected $pass;
    protected $db;

    private $dsn;
    private $options;
    /**
     * This will be the mysqli object
     * @var PDO
     */
    private $conn;

    /**
     * MySqlConnection constructor.
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $db
     * @param string $charset
     */
    public function __construct(string $host, string $user, string $pass,
                                string $db, string $charset = "utf8mb4")
    {
        parent::__construct($host, $user, $pass, $db);
        $this->dsn = "mysql:host=$this->host;dbname=$this->db;charset=$charset";
        $this->options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
    }

    /**
     * Tries to make a new connection to database
     * @return bool
     * @throws PDOException
     */
    public function connect(): bool
    {
        try {
            $this->conn = new PDO($this->dsn, $this->user, $this->pass, $this->options);
            $this->isConnected = true;
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int)$e->getCode());
        }
        return true;
    }

    /**
     * Tries to disconnect from database
     * @return bool
     */
    public function disconnect(): bool
    {
        if ($this->isConnected) {
            $this->conn = null;
            return true;
        }
        return false;
    }

    /**
     * @return PDO
     */
    public function getConn(): PDO
    {
        //debug_print_backtrace();
        return $this->conn;
    }

}