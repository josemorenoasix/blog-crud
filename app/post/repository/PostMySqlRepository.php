<?php

/**
 * Class PostMysqlRepository
 */
class PostMysqlRepository implements PostRepository
{

    /**
     * @var MySqlConnection
     */
    private $repo;

    /**
     * PostMysqlRepository constructor.
     * @param MySqlConnection $db
     */
    public function __construct(MySqlConnection $db)
    {
        $this->repo = $db;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        $q = 'SELECT COUNT(*) FROM `gnr_post`';
        return $this->repo->getConn()->query($q)->fetch_row()[0];
    }

    /**
     * @param int $id
     * @return Post
     * @throws Exception
     */
    public function readById(int $id): Post
    {
        $q = "SELECT * FROM `gnr_post` WHERE `post_id` = ?";
        $stmt = $this->repo->getConn()->prepare($q);
        if (!$stmt->bind_param("i", $id)) {
            throw new Exception("id parameter not valid");
        }

        $stmt->execute();
        $row = $stmt->get_result()->fetch_assoc();
        $stmt->close();

        if (!$row) {
            throw new Exception("Post not found");
        }
        return $this->getPost($row);
    }

    /**,
     * @param array $row
     * @return Post
     */
    private function getPost(array $row)
    {
        return new Post(
            $row["post_title"],
            $row["post_slug"],
            $row["post_abstract"],
            $row["post_body"],
            $row["post_date"],
            ($row["post_visible"] == "Mostrar" ? true : false),
            $row["post_image"],
            $row["post_id"]
        );
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return PostList
     */
    public function readList(int $offset = 0, int $limit = 6): PostList
    {
        $q = "SELECT * FROM `gnr_post` ORDER BY `post_date` DESC LIMIT ? OFFSET ?";
        $stmt = $this->repo->getConn()->prepare($q);
        if (!$stmt->bind_param("ii", $limit, $offset)) {
            throw new Error("parameters are not valid");
        }
        $stmt->execute();
        $rows = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $stmt->close();

        $posts = new PostList();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $posts->addPost($this->getPost($row));
            }
        }
        return $posts;
    }

    /**
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function removeById(int $id): bool
    {
        $q = "DELETE FROM `gnr_post` WHERE `post_id` = ?";
        $stmt = $this->repo->getConn()->prepare($q);
        if (!$stmt->bind_param("i", $id)) {
            throw new Exception("id parameter not valid");
        }
        $stmt->execute();

        if ($stmt->affected_rows == 0) {
            throw new Exception("Post not found");
        }
        $stmt->close();
        return true;
    }

    /**
     * @param Post $post
     * @return int
     * @throws Exception
     */
    public function save($post): int
    {
        if ($post->getId() == 0) {
            return $this->insert($post);
        }
        return $this->update($post);
    }

    /**
     * @param Post $post
     * @return int
     */
    private function insert(Post $post): int
    {
        $q = "INSERT INTO `gnr_post` (`post_title`, `post_slug`, `post_abstract`, `post_body`, `post_date`, `post_visible`, `post_image`) VALUES (?,?,?,?,?,?,?)";
        $arr = $this->convertToArray($post);

        $stmt = $this->repo->getConn()->prepare($q);
        $stmt->bind_param(
            "sssssss",
            $arr["title"],
            $arr["slug"],
            $arr["abstract"],
            $arr["body"],
            $arr["date"],
            $arr["visible"],
            $arr["image"]);

        $stmt->execute();
        $insertId = $stmt->insert_id;
        $stmt->close();

        return $insertId;
    }

    /**
     * @param Post $post
     * @return array
     */
    private function convertToArray(Post $post)
    {

        $arr = [
            "title" => $post->getTitle(),
            "slug" => $post->getSlug(),
            "abstract" => $post->getAbstract(),
            "body" => $post->getBody(),
            "date" => $post->getDate(),
            "visible" => ($post->isActive() ? "Mostrar" : "No Mostrar"),
            "image" => $post->getImage()
        ];

        if ($post->getId() != 0) {
            $arr["id"] = $post->getId();
        }

        return $arr;

    }

    /**
     * @param Post $post
     * @return int
     * @throws Exception
     */
    private function update(Post $post): int
    {
        $q = "UPDATE `gnr_post` SET `post_title` = ?, `post_slug` = ?, `post_abstract` = ?, `post_body` = ?, `post_date` = ?, `post_visible` = ?, `post_image` = ? WHERE `post_id` = ? ";
        $arr = $this->convertToArray($post);

        $stmt = $this->repo->getConn()->prepare($q);

        $stmt->bind_param(
            "sssssssi",
            $arr["title"],
            $arr["slug"],
            $arr["abstract"],
            $arr["body"],
            $arr["date"],
            $arr["visible"],
            $arr["image"],
            $arr["id"]);

        $stmt->execute();

        if ($stmt->affected_rows == 0) {
            throw new Exception("Post not found");
        }
        $stmt->close();
        return $arr["id"];

    }

    /**
     * @param string $slug
     * @return Post
     */
    public function readBySlug($slug): Post
    {
        // TODO: Implement readBySlug() method.
        return null;
    }

}


/*
 *
 * CREATE TABLE IF NOT EXISTS `gnr_post` (
  `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_slug` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_abstract` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_body` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_date` date NOT NULL,
  `post_visible` enum('Mostrar','No Mostrar') NOT NULL,
  `post_image` varchar(200) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=891 ;
 *
 */