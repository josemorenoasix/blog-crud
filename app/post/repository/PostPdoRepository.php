<?php


/**
 * Class PostMysqlRepository
 */
class PostPdoRepository implements PostRepository
{

    /**
     * @var PdoConnection
     */
    private $repo;

    /**
     * PostMysqlRepository constructor.
     * @param PdoConnection $db
     */
    public function __construct(PdoConnection $db)
    {
        $this->repo = $db;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        $q = 'SELECT COUNT(*) FROM `gnr_post`';
        return $this->repo->getConn()->query($q)->fetchColumn();
    }

    /**
     * @param int $id
     * @return Post
     * @throws Exception
     */
    public function readById(int $id): Post
    {
        $q = "SELECT * FROM `gnr_post` WHERE `post_id` = ?";

        $stmt = $this->repo->getConn()->prepare($q);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);

        if ($stmt->execute()) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if (empty($row)) {
                throw new Exception("Post not found");
            }
            return $this->getPost($row);
        }
        return null;
    }

    /**
     * @param array $row
     * @return Post
     */
    private function getPost(array $row)
    {
        return new Post(
            $row["post_title"],
            $row["post_slug"],
            $row["post_abstract"],
            $row["post_body"],
            $row["post_date"],
            ($row["post_visible"] == "Mostrar" ? true : false),
            $row["post_image"],
            $row["post_id"]
        );
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return PostList
     * @throws Exception
     */
    public function readList(int $offset = 0, int $limit = 6): PostList
    {
        $q = "SELECT * FROM `gnr_post` ORDER BY `post_date` DESC LIMIT ? OFFSET ?";
        $stmt = $this->repo->getConn()->prepare($q);
        $stmt->bindParam(1, $limit, PDO::PARAM_INT);
        $stmt->bindParam(2, $offset, PDO::PARAM_INT);

        if ($stmt->execute()) {

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (empty($rows)) {
                throw new Exception("No posts found");
            }

            $posts = new PostList();
            foreach ($rows as $row) {
                $posts->addPost($this->getPost($row));
            }

            return $posts;
        }
        return null;
    }

    /**
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function removeById(int $id): bool
    {
        $q = "DELETE FROM `gnr_post` WHERE `post_id` = ?";

        $stmt = $this->repo->getConn()->prepare($q);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            if ($stmt->rowCount() == 0) {
                throw new Exception("Post not found");
            }
            return true;
        }
        return false;
    }

    /**
     * @param Post $post
     * @return int
     * @throws Exception
     */
    public function save($post): int
    {
        if ($post->getId() == 0) {
            return $this->insert($post);
        }
        return $this->update($post);
    }

    /**
     * @param Post $post
     * @return int
     */
    private function insert(Post $post): int
    {
        $q = "INSERT INTO `gnr_post` (`post_title`, `post_slug`, `post_abstract`, `post_body`, `post_date`, `post_visible`, `post_image`) VALUES (?,?,?,?,?,?,?)";
        $arr = $this->convertToArray($post);

        $stmt = $this->repo->getConn()->prepare($q);
        $stmt->bindParam(1, $arr["title"], PDO::PARAM_STR);
        $stmt->bindParam(2, $arr["slug"], PDO::PARAM_STR);
        $stmt->bindParam(3, $arr["abstract"], PDO::PARAM_STR);
        $stmt->bindParam(4, $arr["body"], PDO::PARAM_STR);
        $stmt->bindParam(5, $arr["date"], PDO::PARAM_STR);
        $stmt->bindParam(6, $arr["visible"], PDO::PARAM_STR);
        $stmt->bindParam(7, $arr["image"], PDO::PARAM_STR);

        if ($stmt->execute()) {
            return $this->repo->getConn()->lastInsertId();
        };

        return null;
    }

    /**
     * @param Post $post
     * @return array
     */
    private function convertToArray(Post $post)
    {

        $arr = [
            "title" => $post->getTitle(),
            "slug" => $post->getSlug(),
            "abstract" => $post->getAbstract(),
            "body" => $post->getBody(),
            "date" => $post->getDate(),
            "visible" => ($post->isActive() ? "Mostrar" : "No Mostrar"),
            "image" => $post->getImage()
        ];

        if ($post->getId() != 0) {
            $arr["id"] = $post->getId();
        }

        return $arr;

    }

    /**
     * @param Post $post
     * @return int
     * @throws Exception
     */
    private function update(Post $post): int
    {
        $q = "UPDATE `gnr_post` SET `post_title` = ?, `post_slug` = ?, `post_abstract` = ?, `post_body` = ?, `post_date` = ?, `post_visible` = ?, `post_image` = ? WHERE `post_id` = ? ";
        $arr = $this->convertToArray($post);

        $stmt = $this->repo->getConn()->prepare($q);
        $stmt->bindParam(1, $arr["title"], PDO::PARAM_STR);
        $stmt->bindParam(2, $arr["slug"], PDO::PARAM_STR);
        $stmt->bindParam(3, $arr["abstract"], PDO::PARAM_STR);
        $stmt->bindParam(4, $arr["body"], PDO::PARAM_STR);
        $stmt->bindParam(5, $arr["date"], PDO::PARAM_STR);
        $stmt->bindParam(6, $arr["visible"], PDO::PARAM_STR);
        $stmt->bindParam(7, $arr["image"], PDO::PARAM_STR);
        $stmt->bindParam(8, $arr["id"], PDO::PARAM_INT);

        if ($stmt->execute()) {
            if ($stmt->rowCount() == 0) {
                throw new Exception("Post not found");
            }
            return $this->repo->getConn()->lastInsertId();
        };

        return null;

    }

    /**
     * @param string $slug
     * @return Post
     */
    public function readBySlug($slug): Post
    {
        // TODO: Implement readBySlug() method.
        return null;
    }

}


/*
 *
 * CREATE TABLE IF NOT EXISTS `gnr_post` (
  `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_slug` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_abstract` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_body` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `post_date` date NOT NULL,
  `post_visible` enum('Mostrar','No Mostrar') NOT NULL,
  `post_image` varchar(200) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=891 ;
 *
 */