<?php

abstract class DbConnection implements DbConnectionInterface
{
    protected $host;
    protected $user;
    protected $pass;
    protected $db;
    /**
     * Check to see if the connection is active
     * @var bool
     */
    protected $isConnected = false;
    /**
     * Query results will be stored here
     * @var array
     */
    protected $result = array();

    /**
     * MySqlConnection constructor.
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $db
     */
    public function __construct($host, $user, $pass, $db)
    {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->db = $db;
    }

    /**
     * Tries to make a new connection to database
     * @return bool
     */
    abstract function connect(): bool;

    /**
     * Tries to disconnect from database
     * @return bool
     */
    abstract function disconnect(): bool;

    /**
     * @return mysqli|PDO
     */
    abstract function getConn();

}