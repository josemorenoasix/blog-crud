<?php


interface DbConnectionInterface
{
    /**
     * Tries to make a new connection to database
     * @return bool
     */
    public function connect(): bool;

    /**
     * Tries to disconnect from database
     * @return bool
     */
    public function disconnect(): bool;

    /**
     * @return PDO|mysqli
     */
    public function getConn();
}