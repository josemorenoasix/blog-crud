<?php

interface PostRepository
{

    /**
     * @param Post $post
     * @return int
     */
    public function save(Post $post): int;

    /**
     * @param int id
     * @return bool
     */
    public function removeById(int $id): bool;


    /**
     * @return int
     */
    public function count(): int;

    /**
     * @param int $id
     * @return Post
     */
    public function readById(int $id): Post;

    /**
     * @param string $slug
     * @return Post
     */
    public function readBySlug(string $slug): Post;

    /**
     * @param int $limit
     * @param int $offset
     * @return PostList
     */
    public function readList(int $offset = 0, int $limit = 6): PostList;


}