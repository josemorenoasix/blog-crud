<?php

class FormPostView
{
    /**
     * @var Post
     */
    private $post;

    /**
     * ReadPostView constructor.
     * @param Post $post
     */
    public function __construct(Post $post = null)
    {
        $this->post = $post;
    }

    /**
     * @param Post $post
     */
    public function setPost(Post $post): void
    {
        $this->post = $post;
    }


    /**
     * @return string
     */
    public function output()
    {

        $image = "./assets/img/400x300.png";
        $title = "";
        $abstract = "";
        $body = "";
        $status = "checked";
        $required = "required";
        if (!is_null($this->post)) {
            $image = "./img/" . $this->post->getImage();
            $title = $this->post->getTitle();
            $abstract = $this->post->getAbstract();
            $body = $this->post->getBody();
            $status = ($this->post->isActive() ? "checked" : "");
            $required = "";
        }
        return
            <<<TAG
<div class="col-md-12">
    <form method="post" enctype="multipart/form-data">
        <input type="hidden" name="MAX_FILE_SIZE" value="1000000"/>

        <div class="row justify-content-center">
            <div class="form-group col-md-10 mb-3">
                <input required name="input-title" type="text" class="form-control" placeholder="Title..." value="$title">
            </div>
            <div class="form-group col-md-2 mb-3">
                <input $status name="input-active" type="checkbox" class="form-control" id="toggle-visible"
                       data-toggle="toggle" data-on="Active" data-off="Disabled"
                       data-onstyle="success" data-offstyle="danger"/>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="form-group col-md-12 mb-3">
                <textarea required name="input-summary" class="form-control" placeholder="Resumen...">$abstract</textarea>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="form-group col-md-10 mb-3 border rounded">
                <div>
                    <img id="preview-image" class="upload-preview img-thumbnail img-fluid mx-auto d-block" src="$image" alt="your image" />
                </div>

            </div>
            <div class="form-group col-md-10 mb-3">
                <label class="custom-file-label" for="input-image" id="label-upload">Choose file</label>
                <input $required id="input-image" name="input-image" type="file" class="custom-file-input"
                       accept="image/png, image/jpeg, image/gif" onchange="readURL(this);"/>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12 mb-3">
                <textarea class="form-control" title="editor" id="summernote" name="input-body" required tabindex="">$body</textarea>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 mb-3">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Save Post</button>
            </div>
        </div>
    </form>
</div>
TAG;
    }
}






