<?php

/**
 * Class ListPostView
 */
class ListPostView
{
    /**
     * @var PostList
     */
    private $posts;
    /**
     * @var int
     */
    private $lastPage;
    /**
     * @var int
     */
    private $currentPage;

    /**
     * ListPostView constructor.
     * @param PostList $posts
     * @param int $lastPage
     * @param int $currentPage
     */
    public function __construct(PostList $posts, int $lastPage, int $currentPage)
    {
        $this->posts = $posts;
        $this->lastPage = $lastPage;
        $this->currentPage = $currentPage;
    }

    /**
     * @return string
     */
    public function output(): string
    {

        $out = $this->html_navigation();

        foreach ($this->posts->getPosts() as $post) {
            $out .= $this->html_card_post($post);
        }

        return $out;
    }

    /**
     * @return string
     */
    private function html_navigation(): string
    {

        $html = "";

        $previous = $this->currentPage - 1;
        $next = $this->currentPage + 1;

        $disabledPrevious = "";
        $disabledNext = "";
        if ($this->currentPage == 1) {
            $disabledPrevious = "disabled";
        }
        if ($this->currentPage == $this->lastPage) {
            $disabledNext = "disabled";
        }

        $html .=
            <<<TAG
<div class="col-md-12">
  <nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
      <li class="page-item $disabledPrevious">
        <a class="page-link" href="?page=$previous" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
TAG;
        for ($i = 1; $i <= $this->lastPage; $i++) {
            $active = "";
            if ($this->currentPage == $i) {
                $active = "active";
            }
            $html .=
                <<<TAG
      <li class="page-item $active"><a class="page-link" href="?page=$i">$i</a></li>
TAG;
        }
        $html .=
            <<<TAG
      <li class="page-item $disabledNext" >
        <a class="page-link" href="?page=$next" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
</div>
TAG;
        return $html;
    }

    /**
     * @param Post $post
     * @return string
     */
    private function html_card_post(Post $post): string
    {
        $id = $post->getId();
        $image = $post->getImage();
        $title = $post->getTitle();
        $abstract = $post->getAbstract();
        $date = $post->getDate();

        $out = <<<TAG
<div class="col-md-6 d-flex align-items-stretch">
    <div class="card mb-6 h-100 w-100 box-shadow">
        <a class="page-link" href="?action=read&id=$id">
        <img class="card-img-top img-thumbnail" src="img/$image" alt="Card image cap">
        </a>
        
        <div class="card-body">
        <a href="?action=read&id=$id">
            <h5 class="card-title">$title</h5>
        </a>
            <p class="card-text">$abstract</p>                    
        </div>

        <div class="card-footer text-muted">
            <div class="d-flex justify-content-between align-items-center">
                  <div class="btn-group">
                      <a role="button" href="?action=read&id=$id" class="btn btn-sm btn-outline-secondary">Read</a>
                      <a role="button" href="?action=edit&id=$id" class="btn btn-sm btn-outline-secondary">Edit</a>
                      <button id="delete-button" data-toggle="modal" data-target="#deleteAlert" class="btn btn-sm btn-outline-secondary" value="$id">Delete</a>
                  </div>
                  <small class="text-muted">$date</small>
            </div>
        </div>
    </div>
</div>
TAG;
        return $out;

    }

}
