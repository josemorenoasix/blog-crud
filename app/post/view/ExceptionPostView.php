<?php

/**
 * Class ExceptionPostView
 */
class ExceptionPostView
{
    /**
     * @var string
     */
    private $action;
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $type;


    /**
     * ExceptionPostView constructor.
     * @param string $action
     * @param string $message
     * @param string $type
     */
    public function __construct(string $action, string $message, string $type)
    {
        $this->action = $action;
        $this->message = $message;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function output(): string
    {
        return <<<TAG
<div class="alert $this->type" role="alert">
  <strong>$this->action</strong>$this->message
</div>
TAG;
    }


}