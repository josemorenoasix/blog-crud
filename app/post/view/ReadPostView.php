<?php

/**
 * Class ReadPostView
 */
class ReadPostView
{
    /**
     * @var Post
     */
    private $post;

    /**
     * ReadPostView constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @return string
     */
    public function output(): string
    {
        $id = $this->post->getId();
        $image = $this->post->getImage();
        $title = $this->post->getTitle();
        $abstract = $this->post->getAbstract();
        $date = $this->post->getDate();
        $body = $this->post->getBody();
        return
            <<<TAG
            <div class="col-md-12">
            <img class="" src="img/$image" alt="Image cap">
                    <div class="">
                        <h5 class="">$title</h5>
                        <p class="">$abstract</p>                    
                    </div>
                    <div class="">
                        $body                    
                    </div>                    
                    <div class="">
                        <div class="d-flex justify-content-between align-items-center">
                              <div class="btn-group">
                                    <a role="button" href="?action=read&id=$id" class="btn btn-sm btn-outline-secondary">Read</a>
                                    <a role="button" href="?action=edit&id=$id" class="btn btn-sm btn-outline-secondary">Edit</a>
                                    <button id="delete-button" data-toggle="modal" data-target="#deleteAlert" class="btn btn-sm btn-outline-secondary" value="$id">Delete</a>
                              </div>
                              <small class="text-muted">$date</small>
                        </div>
                    </div>
            </div>
TAG;
    }
}