<?php


/**
 * Class PostController
 */
class PostController
{

    /**
     * @var PostRepository
     */
    protected $repo;

    /**
     * PostController constructor.
     * @param PostRepository $repo
     */
    public function __construct(PostRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @param array $a
     * @return bool
     */
    protected function isValidId($a): bool
    {
        return !(!isset($a['id']) || empty($a['id']) || !is_numeric($a['id']) || (int)$a['id'] < 1);
    }

    // TODO: implement min and max values

    /**
     * @param array $form
     * @return bool
     */
    protected function isValidForm(array $form): bool
    {
        return
            (isset($form['input-title']) && !empty($form['input-title'])) && !ctype_space($form['input-title']) &&
            (isset($form['input-summary']) && !empty($form['input-summary'])) && !ctype_space($form['input-summary']) &&
            (isset($form['input-body']) && !empty($form['input-body'])) && !ctype_space($form['input-body']);
    }

    /**
     * @param $string
     * @return string
     */
    protected function doSlug(string $string)
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
    }

}