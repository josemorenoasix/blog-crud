<?php

/**
 * Class CreatePostController
 */
class CreatePostController extends PostController
{

    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * CreatePostController constructor.
     * @param PostRepository $repo
     * @param ImageUploader $imageUploader
     */
    public function __construct(PostRepository $repo, ImageUploader $imageUploader)
    {
        parent::__construct($repo);
        $this->imageUploader = $imageUploader;
    }

    /**
     * @param array $data_POST
     * @return int
     * @throws Exception
     */
    public function createPost(array $data_POST): int
    {
        if ($this->imageUploader->offsetGet("input-image") && $this->isValidForm($data_POST)) {
            $upload = $this->imageUploader->upload();
            if ($upload) {
                try {
                    return $this->repo->save(new Post(
                            $data_POST['input-title'],
                            $this->doSlug($data_POST['input-title']),
                            $data_POST['input-summary'],
                            $data_POST['input-body'],
                            date("Y-m-d"),
                            (isset($data_POST['input-active']) ? true : false),
                            $this->imageUploader->getName() . '.' . $this->imageUploader->getMime())
                    );
                } catch (Exception $e) {
                    throw new Exception("Database insert post failed: " . $e->getMessage());
                }
            } else {
                throw new Exception("Image upload failed");
            }
        } else {
            throw new Exception("Invalid form inputs");
        }
    }

}