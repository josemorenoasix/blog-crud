<?php

require_once("PostController.php");


/**
 * Class UpdatePostController
 */
class UpdatePostController extends PostController
{

    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * UpdatePostController constructor.
     * @param PostRepository $repo
     */
    public function __construct(PostRepository $repo)
    {
        parent::__construct($repo);
    }

    /**
     * @param ImageUploader $imageUploader
     */
    public function setImageUploader(ImageUploader $imageUploader): void
    {
        $this->imageUploader = $imageUploader;
    }

    /**
     * @param array $data_GET
     * @param array $data_POST
     * @return int
     * @throws Exception
     */
    public function updatePost(array $data_GET, array $data_POST)
    {

        if (!$this->isValidId($data_GET)) {
            throw new Exception("Invalid Id");
        }

        if ($this->isValidForm($data_POST)) {

            if (isset($this->imageUploader) && !empty($this->imageUploader->offsetGet("input-image"))) {
                $upload = $this->imageUploader->upload();
                if (!$upload) {
                    throw new Exception("Image upload failed");
                }
            }

            try {
                $oldPost = $this->repo->readById($data_GET["id"]);
                return $this->repo->save(
                    new Post(
                        $data_POST['input-title'],
                        $this->doSlug($data_POST['input-title']),
                        $data_POST['input-summary'],
                        $data_POST['input-body'],
                        date("Y-m-d"),
                        (isset($data_POST['input-active']) ? true : false),
                        (isset($upload) ? $this->imageUploader->getName() . '.' . $this->imageUploader->getMime() : $oldPost->getImage()),
                        $oldPost->getId()
                    )
                );
            } catch (Exception $e) {
                throw new Exception("Database update post failed: " . $e->getMessage());
            }
        } else {
            throw new Exception("Invalid form inputs");
        }
    }


    /**
     * @param array $data_GET
     * @return Post
     * @throws Exception
     */
    public function readPost(array $data_GET): Post
    {
        if (!$this->isValidId($data_GET)) {
            throw new Exception("Invalid Id");
        }
        return $this->repo->readById((int)$data_GET['id']);
    }

}