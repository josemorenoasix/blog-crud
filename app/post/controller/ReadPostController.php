<?php

require_once("PostController.php");

/**
 * Class ReadPostController
 */
class ReadPostController extends PostController
{

    /**
     * ReadPostController constructor.
     * @param PostRepository $repo
     */
    public function __construct(PostRepository $repo)
    {
        parent::__construct($repo);
    }

    /**
     * @param $data_GET
     * @return Post
     * @throws Exception
     */
    public function readPost($data_GET): Post
    {
        if (!$this->isValidId($data_GET)) {
            throw new Exception("Invalid Id");
        }
        return $this->repo->readById((int)$data_GET['id']);
    }

}