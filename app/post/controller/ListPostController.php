<?php

define('MAX_PAGE_SIZE', 12);
define('DEFAULT_PAGE_SIZE', 6);
define('MIN_PAGE_SIZE', 4);

/**
 * Class ListPostController
 */
class ListPostController extends PostController
{
    /**
     * @var int
     */
    private $currentPage;
    /**
     * @var int
     */
    private $sizePage;
    /**
     * @var int
     */
    private $lastPage;


    /**
     * ListPostController constructor.
     * @param PostRepository $repo
     */
    public function __construct(PostRepository $repo)
    {
        parent::__construct($repo);
    }

    /**
     * @param array $dataGET
     */
    public function calcPagination(array $dataGET)
    {
        $this->sizePage = $this->setSizeOfPage($dataGET);
        $this->lastPage = ceil($this->repo->count() / $this->getSizePage());
        $this->currentPage = $this->setCurrentPage($dataGET);
    }

    /**
     * @param array $a
     * @return int
     */
    private function setSizeOfPage($a)
    {
        if (!isset($a['size']) || (int)$a['size'] < MIN_PAGE_SIZE || (int)$a['size'] > MAX_PAGE_SIZE) {
            return DEFAULT_PAGE_SIZE;
        }
        return (int)$a['size'];

    }

    /**
     * @return int
     */
    public function getSizePage(): int
    {
        return $this->sizePage;
    }

    /**
     * @param array $a
     * @return int
     */
    private function setCurrentPage($a)
    {
        if (!isset($a['page']) || (int)$a['page'] < 1 || (int)$a['page'] > $this->lastPage) {
            return 1;
        }
        return (int)$a['page'];
    }

    /**
     * @return PostList
     */
    public function getPostList(): PostList
    {
        $offset = ($this->currentPage - 1) * $this->sizePage;
        return $this->repo->readList($offset, $this->sizePage);
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return int
     */
    public function getLastPage(): int
    {
        return $this->lastPage;
    }

}