<?php

/**
 * Class DeletePostController
 */
class DeletePostController extends PostController
{

    /**
     * DeletePostController constructor.
     * @param PostRepository $repo
     */
    public function __construct(PostRepository $repo)
    {
        parent::__construct($repo);
    }


    /**
     * @param array $data_GET
     * @return bool
     * @throws Exception
     */
    public function deletePost(array $data_GET): bool
    {
        if (!$this->isValidId($data_GET)) {
            throw new Exception("Invalid Id");
        }
        return $this->repo->removeById((int)$data_GET['id']);
    }

}