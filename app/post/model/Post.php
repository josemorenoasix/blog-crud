<?php

/**
 * Class Post
 */
class Post
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $slug;
    /**
     * @var string
     */
    private $abstract;
    /**
     * @var string
     */
    private $body;
    /**
     * @var string
     */
    private $date;
    /**
     * @var bool
     */
    private $visible;
    /**
     * @var string
     */
    private $image;


    /**
     * Post constructor.
     * @param int|null $id
     * @param string $title
     * @param string $slug
     * @param string $abstract
     * @param string $body
     * @param string $date
     * @param bool $active
     * @param string|null $image
     */
    public function __construct(string $title, string $slug, string $abstract,
                                string $body, string $date, bool $active, string $image = "", $id = 0)
    {
        $this->title = $title;
        $this->slug = $slug;
        $this->abstract = $abstract;
        $this->body = $body;
        $this->date = $date;
        $this->visible = $active;
        $this->image = $image;
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getAbstract(): string
    {
        return $this->abstract;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->visible;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }


}


