<?php

/**
 * Class PostList
 */
class PostList
{

    /**
     * @var Post[]
     */
    private $posts = [];

    /**
     * PostList constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return Post[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post): void
    {
        array_push($this->posts, $post);
    }

}