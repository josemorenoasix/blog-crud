<?php
/**
 * Created by PhpStorm.
 * User: jmoreno
 * Date: 6/10/18
 * Time: 12:07
 */
ob_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
define('FORM_INPUTS',
    array(
        "input-title",
        "input-summary",
        "input-body",
        "input-status"
    )
);

require_once("post/interface/DbConnectionInterface.php");
require_once("post/interface/PostRepository.php");
require_once("post/repository/DbConnection.php");
require_once("post/repository/MySqlConnection.php");
require_once("post/repository/PdoConnection.php");
require_once("post/repository/PostMySqlRepository.php");
require_once("post/repository/PostPdoRepository.php");
require_once("post/controller/PostController.php");
require_once("post/controller/ListPostController.php");
require_once("post/controller/ReadPostController.php");
require_once("post/controller/CreatePostController.php");
require_once("post/controller/DeletePostController.php");
require_once("post/controller/UpdatePostController.php");
require_once("post/controller/ImageUploader.php");
require_once("post/view/ReadPostView.php");
require_once("post/view/FormPostView.php");
require_once("post/view/ListPostView.php");
require_once("post/model/Post.php");
require_once("post/model/PostList.php");
?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8,">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type='image/x-icon' href="./favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon-16x16.png">

    <title>blog-crud</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
</head>

<body>

<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">About</h4>
                    <p class="text-muted">Jose Moreno</p>
                </div>
                <div class="col-sm-4 offset-md-1 py-4">
                    <h4 class="text-white">Contact</h4>
                    <ul class="list-unstyled">
                        <li><a href="#" class="text-white">Email me</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
            <a href="/" class="navbar-brand d-flex align-items-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2">
                    <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path>
                    <circle cx="12" cy="13" r="4"></circle>
                </svg>
                <strong>blog-crud</strong>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader"
                    aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>

<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Proyecto blog-crud</h1>
            <p class="lead text-muted"> Operaciones CRUD (Create, Read, Update and Delete) utilizando mysqli</p>
            <p>
                <a href="?action=new" class="btn btn-primary my-2"> + New Post</a>
            </p>
        </div>
    </section>
    <?php
    if (isset($_GET["alert"]) && !empty($_GET["alert"])) {
        $message = htmlspecialchars($_GET["alert"]);
        print
            <<<TAG
        <div class="fixed-bottom alert alert-warning alert-dismissible fade show" role="alert">                        
            <strong>Message!</strong> $message
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
TAG;
    }
    ?>
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <?php
                //$db = new MySqlConnection("db","mysqli_post","topSecret","db_post");
                $db = new PdoConnection("db", "mysqli_post", "topSecret", "db_post");
                if ($db->connect()) {
                    //$repo = new PostMySqlRepository($db);
                    $repo = new PostPdoRepository($db);
                    if (isset($_GET['action']) && !empty($_GET['action'])) {
                        switch (htmlspecialchars($_GET['action'])) {
                            case "read":
                                $controller = new ReadPostController($repo);
                                try {
                                    $post = $controller->readPost($_GET);
                                } catch (Exception $e) {
                                    $db->disconnect();
                                    header('Location: /?alert=' . $e->getMessage());
                                    exit();
                                }
                                $view = new ReadPostView($post);
                                echo $view->output();
                                break;

                            case "delete":
                                $controller = new DeletePostController($repo);
                                try {
                                    $controller->deletePost($_GET);
                                } catch (Exception $e) {
                                    $db->disconnect();
                                    header('Location: /?alert=' . $e->getMessage());
                                    exit();
                                }
                                $db->disconnect();
                                header('Location: /?alert=Post deleted');
                                exit();

                            case "new":
                                if (isset($_FILES) && !empty($_FILES) && isset($_POST) && !empty($_POST)) {
                                    $controller = new CreatePostController($repo, new ImageUploader($_FILES));
                                    try {
                                        $insertId = $controller->createPost($_POST);
                                    } catch (Exception $e) {
                                        $db->disconnect();
                                        header('Location: /?alert=' . $e->getMessage());
                                        exit();
                                    }
                                    $db->disconnect();
                                    header('Location: /?action=read&id=' . $insertId . '&alert=Post saved');
                                    exit();
                                }
                                $view = new FormPostView();
                                echo $view->output();
                                break;

                            case "edit":
                                $controller = new UpdatePostController($repo);
                                if (isset($_POST) && !empty($_POST) && isset($_FILES) && !empty($_FILES)) {
                                    if (empty($_FILES["input-image"]["error"])) {
                                        print_r("setting image uploader");
                                        $controller->setImageUploader(new ImageUploader($_FILES));
                                    }
                                    try {
                                        $updateId = $controller->updatePost($_GET, $_POST);
                                    } catch (Exception $e) {
                                        $db->disconnect();
                                        header('Location: /?alert=' . $e->getMessage());
                                        exit();
                                    }
                                    $db->disconnect();
                                    header('Location: /?action=read&id=' . $updateId . '&alert=Post saved');
                                    exit();
                                } else {
                                    try {
                                        $post = $controller->readPost($_GET);
                                    } catch (Exception $e) {
                                        $db->disconnect();
                                        header('Location: /?alert=' . $e->getMessage());
                                        exit();
                                    }

                                }
                                $view = new FormPostView($post);
                                echo $view->output();
                                break;

                            default:
                                $db->disconnect();
                                header('Location: /');
                                exit();
                        }
                    } else {
                        $controller = new ListPostController($repo);
                        $controller->calcPagination($_GET);
                        $view = new ListPostView($controller->getPostList(), $controller->getLastPage(), $controller->getCurrentPage());
                        echo $view->output();
                    }
                    $db->disconnect();
                };
                ob_flush();
                ?>
            </div>
        </div>
    </div>
</main>
<!-- Modal -->
<div class="modal fade" id="deleteAlert" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Are you sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="delete-confirm-button" type="button" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</div>
<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Back to top</a>
        </p>
    </div>
</footer>
</body>
</html>


<script>
    $('#summernote').summernote({
        placeholder: '...',
        minHeight: 200,             // set minimum height of editor
        maxHeight: 400,             // set maximum height of editor
    });

    $(function () {
        $('#delete-confirm-button').click(function () {
            window.location = '?action=delete&id=' + $('#delete-button').val();
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview-image')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);

            $('#label-upload')
                .text(input.files[0].name);

        }
    }
</script>





