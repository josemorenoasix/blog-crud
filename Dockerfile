FROM php:7.2-apache

# Install mysqli native library
RUN docker-php-ext-install mysqli

# Install exif native library for image manipulation
RUN docker-php-ext-install exif

# Install pdo mysql driver
RUN docker-php-ext-install pdo_mysql

# Install xdebug native library and enable remote connections
RUN pecl install xdebug-2.7.0alpha1
RUN docker-php-ext-enable xdebug
RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/php.ini
