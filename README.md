# blog-crud-mysqli
Docker container with Apache, PHP:7-2, MySql:8 and PhpMyAdmin

Use docker-compose as an orchestrator. To run these container:

```
docker-compose up -d
```

That's all!
